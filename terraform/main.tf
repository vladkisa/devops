resource "aws_eks_cluster" "dtw" {
  name            = "${var.cluster-name}"
  role_arn        = "${aws_iam_role.dtw-cluster.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.dtw-cluster.id}"]
    subnet_ids         = flatten(["${aws_subnet.dtw.*.id}"])
  }

  depends_on = [
    "aws_iam_role_policy_attachment.dtw-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.dtw-cluster-AmazonEKSServicePolicy",
  ]
}

#»Worker Node Access to EKS Master Cluster
resource "aws_security_group_rule" "dtw-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.dtw-cluster.id}"
  source_security_group_id = "${aws_security_group.dtw-node.id}"
  to_port                  = 443
  type                     = "ingress"
}

