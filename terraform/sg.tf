# Master Node Security Group
resource "aws_security_group" "dtw-cluster" {
  name        = "terraform-eks-dtw-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.dtw.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-eks-dtw"
  }
}

# OPTIONAL: Allow inbound traffic from your local workstation external IP
#           to the Kubernetes. You will need to replace A.B.C.D below with
#           your real IP. Services like icanhazip.com can help you find this.
resource "aws_security_group_rule" "dtw-cluster-ingress-workstation-https" {
  cidr_blocks       = ["82.144.208.10/32"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.dtw-cluster.id}"
  to_port           = 443
  type              = "ingress"
}

# Worker Node Security Group
resource "aws_security_group" "dtw-node" {
  name        = "terraform-eks-dtw-node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${aws_vpc.dtw.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "terraform-eks-dtw-node",
     "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "dtw-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.dtw-node.id}"
  source_security_group_id = "${aws_security_group.dtw-node.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "dtw-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.dtw-node.id}"
  source_security_group_id = "${aws_security_group.dtw-cluster.id}"
  to_port                  = 65535
  type                     = "ingress"
}
