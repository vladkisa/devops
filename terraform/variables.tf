variable "cluster-name" {
  default = "DTW_EKS_Cluster01"
  type    = "string"
  description = "Name of EKS Cluster"
}

variable "ami_owner" {
  default = "852975369460"
  type    = "string"
  description = "Amazon EKS AMI Account ID"
}

# VPC Configuration
variable "vpc_name" {
  default = "test"
  type    = "string"
  description = "Amazon EKS VPC Name"
}