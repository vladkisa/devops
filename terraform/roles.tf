# IAM Master Role
resource "aws_iam_role" "dtw-cluster" {
  name = "terraform-eks-dtw-cluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "dtw-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.dtw-cluster.name}"
}

resource "aws_iam_role_policy_attachment" "dtw-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.dtw-cluster.name}"
}

# IAM Worker Role
resource "aws_iam_role" "dtw-node" {
  name = "terraform-eks-dtw-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "dtw-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.dtw-node.name}"
}

resource "aws_iam_role_policy_attachment" "dtw-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.dtw-node.name}"
}

resource "aws_iam_role_policy_attachment" "dtw-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.dtw-node.name}"
}

resource "aws_iam_instance_profile" "dtw-node" {
  name = "terraform-eks-dtw"
  role = "${aws_iam_role.dtw-node.name}"
}